from django.test import TestCase

# Create your tests here.
* test POST
    import requests
    import json
    url = 'http://localhost:8000/api/'

    body= {
        "origin": 'Ternopil',
        "destination": "Kyiv",
    }

    jsonBody = json.dumps(body, ensure_ascii=False).encode('utf8')
    result = requests.post(url, jsonBody) # , headers=headers)
    result.json()

    response:
        {'origin_demo': 'Ternopil', 'destination_demo': 'Kyiv'}

* test GET:
    url = 'http://localhost:8000/api/'
    result = requests.get(url)
    result.text
