from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
import json
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
@csrf_exempt # use this in order toi avoid errors
    # @api_view(['GET'])
def call_demo_api(request):
    if request.method == 'GET':
        return HttpResponse('this is HttpResponse to GET request ')
    elif request.method == 'POST':
        data = json.loads(request.body)
        data_res = {'origin_demo': data['origin'],'destination_demo': data['destination']}
        return JsonResponse(data_res)